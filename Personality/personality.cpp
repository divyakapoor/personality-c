<<<<<<< HEAD
// Divya Kapoor and Kathy Bryan
=======
// Divya Kapoor and Kathya Bryan
>>>>>>> 19de57f6e78d1ef69e9bd8b678607ce583eef8a8
// 10/2/19
// TCES 203
// Project #2
//
// This program will read an input file containing the response of people to the
// Keirsey Temperament Sorter Test and produce a output file containing the personality types


#include<fstream>
#include<string>
#include<cstring>

using namespace std;
const int FOUR = 4;

void answerCount(int numOfAs[], int numOfBs[], char ch, int i);
void personalityResults(char personalityTypes[], int percentsOfB[]);
void outputFile(ofstream& file, char name[], int percentsOfB[], char personalityTypes[]);
void computePercent(int numOfAs[],int numOfBs[],int percentsOfB[]);

int main()
{
ifstream inputFile("personality.txt");
ofstream outFile("personality.out");
char names[80];
char answers[80];

while(inputFile.getline(names, 80))
{
int numOfAs[FOUR] = {0};
int numOfBs[FOUR] = {0};
int percentsOfB[FOUR] = {0};
char personalityTypes[FOUR];

inputFile.getline(answers, 80);
char *ptr = answers;

for (int i=0; i<strlen(answers); i+=7)
{
answerCount(numOfAs, numOfBs, *(ptr + i ), 0);
answerCount(numOfAs, numOfBs, *(ptr + i + 1), 1);
answerCount(numOfAs, numOfBs, *(ptr + i + 2), 1);
answerCount(numOfAs, numOfBs, *(ptr + i + 3), 2);
answerCount(numOfAs, numOfBs, *(ptr + i + 4), 2);
answerCount(numOfAs, numOfBs, *(ptr + i + 5), 3);
answerCount(numOfAs, numOfBs, *(ptr + i + 6), 3);
}
computePercent(numOfAs, numOfBs, percentsOfB);
personalityResults(personalityTypes, percentsOfB);
outputFile(outFile, names, percentsOfB, personalityTypes);
}
outFile.close();
return 0;
}

// This function updates the count of A or B answer based
//on the input character at the entered index

void answerCount(int numOfAs[], int numOfBs[], char ch, int i)
{
if (ch == 'A' || ch == 'a') {
numOfAs[i]++;
} else if (ch == 'B' || ch == 'b') {
numOfBs[i]++;
}
}


// the function computes the percentages of B(rounded to next integer)
//based on the count of A and B responses

void computePercent(int numOfAs[],int numOfBs[], int percentsOfB[])
{
for (int i=0; i<FOUR; i++)
{
int total = numOfAs[i] + numOfBs[i];
float percentage = (float) numOfBs[i] * 100 / (float) total;
percentsOfB[i] = (int)(percentage + 0.5);
}
}

// This function updates the personlity types for each of the 4 sets
//depending on the percentages of B

void personalityResults(char personalityTypes[], int percentsOfB[])
{
if (percentsOfB[0] > 50)
{
personalityTypes[0] = 'I';
} else if(percentsOfB[0] < 50)
{
personalityTypes[0] = 'E';
} else
{
personalityTypes[0] = 'X';
}

if (percentsOfB[1] > 50)
{
personalityTypes[1] = 'N';
} else if(percentsOfB[1] < 50)
{
personalityTypes[1] = 'S';
} else {
personalityTypes[1] = 'X';
}

if (percentsOfB[2] > 50)
{
personalityTypes[2] = 'F';
} else if(percentsOfB[2] < 50)
{
personalityTypes[2] = 'T';
} else
{
personalityTypes[2] = 'X';
}

if (percentsOfB[3] > 50)
{
personalityTypes[3] = 'P';
} else if(percentsOfB[3] < 50)
{
personalityTypes[3] = 'J';
} else
{
personalityTypes[3] = 'X';
}
}

// The function write the final output to the file specified, one line at a time

void outputFile(ofstream& file, char name[], int percentsOfB[], char personalityTypes[])
{
file << name << ": [";
for (int i=0; i<=2; i++)
{
file << percentsOfB[i] << ", ";
}
file << percentsOfB[3] << "] = ";
for (int i=0; i<FOUR; i++)
{
file << personalityTypes[i];
}
file << endl;
}
